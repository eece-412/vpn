#simple GUI

from Tkinter import *
import socket, threading
from vpn import *


#create the window

root = Tk()
connection = Vpn()

#varaibles
targetaddress = StringVar()
portaddress = StringVar()
message=StringVar()
ssv=StringVar()
global remsg
remsg=StringVar() #variable for received msg


#modify root window
root.title("EECE412 VPN")
root.geometry("500x250")

#callbacks
def cbuttoncommand():
  targetentry.configure(state="normal")
  targetentry.update
  #label5.grid_remove()
  connection.mode = 0

def sbuttoncommand():
  targetentry.configure(state="disable")
  targetentry.update
  connection.mode = 1 

def sendmsgbutton():
  remsg=str(message.get())
  connection.sendMessage(remsg)
  print remsg
  
def connectButton():
  if connection.mode == 1:
    if (portaddress.get() == "") or (ssv.get() == ""):
      print "You are missing Port address or Shared Secret Key!"
      return
  elif (targetaddress.get() == "") or (portaddress.get() == "") or (ssv.get() == ""):
    print "You are missing Target address, Port address, or Shared Secret Key!"
    return
  if connection.mode == 0:
    connection.vpnClientThread()
  if connection.mode == 1:
    connection.vpnServerThread()

  connection.keyAB = str(ssv.get()).zfill(16)
  connection.host = str(targetaddress.get())
  connection.port = int(portaddress.get())
  t = threading.Thread(target=updateRemsg)
  t.start()

def updateRemsg():
  message = ''
  while(connection.run):
    if message != connection.msg:
      message = connection.msg
      label7.config(text=message)
      label7.grid()

def disconnect():
  connection.disconnect()

def quit():
  connection.run = False
  root.destroy()


#labels
label1=Label(root,text= "Please select Client or Server", fg= 'red').grid(row=0, column=0)
label2=Label(root, text="Target Address").grid(row=5, column=0 )
label3=Label(root, text= "Port Address").grid(row=6, column=0)
label4=Label(root, text= "Enter Message").grid(row=8, column=0)
label9=Label(root, text= "Shared Secret Value").grid(row=7,column=0)
#label5=Label(root, text="Server waiting for connection").grid(row=11, column=1)

label6=Label(root, text= "Received Message:").grid(row=9, column=0)
label7=Label(root, text=remsg)
label7.grid(row=9, column=1)
label7.grid_remove()

#buttons
clientbutton = Button(root, text = "Client", command=cbuttoncommand).grid(row=1, column=0)

serverbutton = Button(root, text = "Server", command=sbuttoncommand).grid(row=1, column=1)

terminatebutton = Button(root, text ="Disconnect", command=disconnect).grid(row= 10, column=0)
quitbutton = Button(root, text ="Quit", command=quit).grid(row= 11, column=0)
sendbutton = Button(root, text="Send Message", command=sendmsgbutton).grid(row=8, column=2)
connectbutton = Button(root, text="Connect", command=connectButton).grid(row=7, column=2)
#entry field
targetentry= Entry(root, textvariable= targetaddress)
targetentry.grid(row=5, column=1)
portentry= Entry(root, textvariable= portaddress).grid(row=6, column=1)
ssventry = Entry(root, textvariable = ssv).grid(row=7, column=1)
msgentry=Entry(root, textvariable= message).grid(row=8,column=1)



root.mainloop()

