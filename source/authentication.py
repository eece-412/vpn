# CHECK DEPENDENCIES
import socket, threading
from Crypto.Cipher import AES
from Crypto.Hash import md5
from Crypto import Random
from Crypto.Random import random

PORT = 50080
BUFSIZE = 4096  #MESSAGES ARE IN CHAR


class auth():
	def clientInit(self):
 		self.a = self.conv(random.StrongRandom().getrandbits(32), 4)
		self.keyAB = self.conv(random.StrongRandom().getrandbits(256), 32)
		self.rA = self.conv(random.StrongRandom().getrandbits(256), 32)
		
		h = MD5.new()
		h.update('client'.encode('utf-8'))
		self.clienthash = h.hexdigest()
		return self.rA + self.clienthash + self.keyAB

	def serverInit(self,clientmsg):
		self.g=20	#base
 		self.p=91744613	#prime number
 		self.Rb=random.StrongRandom().getrandbits(16)
 		self.b=random.StrongRandom().getrandbits(16)
 		self.iv=random.StrongRandom().getrandbits(16)
	 
 		#divide clientmsg
 		rA=clientmsg[0:128]
 		clienthash=clientmsg[128:192]
 		keyAB=clientmsg[192:448]
 		gdb=(g**int(b,16))%p #for DH

 		#get md5 value for server
		m=md5.new()
		m.update('server')
		serverMD5=m.digest()
	 
 		#Concatenate serverMD5, Ra(client to verify), gbp(for DH) 
 		msg=serverMD5+Ra+gbp

 		#Encrypt the msg above before sending to client
 		cipher=AES.new(key,AES.MODE_CBC,iv)
 		msgEncrypted=cipher.encrypt(msg)
 		serverkey=g+p+Rb+iv+msgEncryped
		return serverkey

	def clientReply(self):

		# RETREIVING MSG (1, 8, 64, 128)
		self.g = int(cmsg[0:1])
		self.p = int(cmsg[1:9])
		iv = cmsg[9:75]
		self.rB = cmsg[75:203]
		ciphertext = cmsg[203:]

  	# DECRYPTING ciphertext and get serverHash, rA, gbp
		decryptor = decryptCiphertext(self.keyAB, AES.MODE_CBC, iv)
		plaintext = decryptor.decrypt(ciphertext)       
		serverhash = plaintext[0:64]
		rA = plaintext[64:192]
		gbp = plaintext[192:]
        
		# Checking rA
		if self.rA != rA: 
			print('Incorrect rA')
			return False       
        
		# Checking serverHash
		md=md5.new()
		m.update('server')
		serverMD5=m.digest()
		serverMD5_orig = m.hexdigest()
		if serverMD5_orig != serverhash: 
			print('server hash incorrect')
			return False

 		# Computing session key Ks using 'g^b mod p' and 'a'          
		self.sessionKey = int(gbp)**int(self.a, 16) % self.p     
		return True 

	def serverReply(self,clientkey):
		decryptor=AES.new(key,AES.MODE_CBC,iv) #where is iv from ?
		plaintext=decryptor.decrypt(clientkey)
	 
		clientMD5=plaintext[0:64]
		Rb=plaintext[64:192]
		gap=plaintext[192:]

		m=md5.new()
		m.update('client')
		clientMD5_orig=m.digest()

		if(clientMD5_orig != clientMD5):
			print('client MD5 is incorrect')
			return False
	 
		if(self.Rb != Rb):
			print('Rb is not correct')
			return False

		self.sessionKey=int(gap)**int(b,16)%p
		return sessionKey

	def encryptMessage(self,msg,sessionKey):
		iv=Random.new().read(AES.block_size)	#get random iv value
		cipher=AES.new(sessionKey, AES.MODE_CBC, iv)	
		ciphermsg=iv+cipher.encrypt(msg)
		return ciphermsg

	def decryptCiphertext(self,ciphermsg,sessionKey): 
		iv=ciphermsg[0:16]
		ciphertext=ciphermsg[16:]
		decryptor=AES.new(sessionKey, AES.MODE_CBC,iv)
		plaintext=decryptor.decrypt(ciphertext)
		return plaintext

	def conv(self, val, digit):
		return str(hex(val)).replace("0x","").replace("L","").zfill(digit)