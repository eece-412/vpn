import socket, threading
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Random import random

class Vpn():
  def __init__(self):
    self.run = True
    self.host = ""
    self.port = 61809
    self.mode = 0
    self.msg = ''
    self.init = 0
    self.keyAB = b'Sixteen byte key'
    self.g=143 #base
    self.p=173 #prime number
    self.t = []

  def vpnServer(self):
    try:
      self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.s.bind((self.host, self.port))
    except:
      raise
    self.s.listen(1)
    while self.run:
      self.conn, self.addr = self.s.accept()
      print 'Connected by', self.addr
      t1 = threading.Thread(target=self.readFromSocket, args=(self.conn,))
      t1.setDaemon(True)
      t1.start()
      self.t.append(t1)

  def readFromSocket(self, socket):
    while self.run:
      data = socket.recv(1024)
      if not data:
        break
      print 'Received', repr(data)
      if self.init < 2: 
        if (self.mode == 0) and (self.init == 1):
          self.s.sendall(self.clientReply(data))
        elif (self.mode == 1) and (self.init == 0):
          self.conn.sendall(self.serverInit(data))
        elif (self.mode == 1) and (self.init == 1):
          self.serverReply(data)
        self.init += 1
      else:
        decryptor = AES.new(self.keyS, AES.MODE_CFB, self.iv)
        plaintext = decryptor.decrypt(data)
        self.msg = plaintext
      # conn.send(data)

  # Echo client program
  def vpnClient(self):
    self.init = 1
    try:
      self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.s.connect((self.host, self.port))
    except:
      raise
    t1 = threading.Thread(target=self.readFromSocket, args=(self.s,))
    t1.setDaemon(True)
    t1.start()
    self.t.append(t1)
    self.s.sendall(self.clientInit())

  def vpnServerThread(self):
    print "Running Server thread"
    t = threading.Thread(target=self.vpnServer)
    t.start()

  def vpnClientThread(self):
    print "Running Client thread"
    t = threading.Thread(target=self.vpnClient)
    t.start()

  def sendMessage(self, msg):
    cipher = AES.new(self.keyS,AES.MODE_CFB,self.iv)
    encryptedMsg = cipher.encrypt(msg)
    if self.mode == 0:
      self.s.sendall(encryptedMsg)
    else:
      self.conn.sendall(encryptedMsg)

  def disconnect(self):
    print "Disconnected"
    self.s.close()
    for thread in self.t:
      thread.join()

  def clientInit(self):
    self.a = random.randrange(30, 101)
    self.rA= str(random.randrange(30, 101))
    self.clientID = random.randrange(30, 101)
    return str(self.clientID) + "," + self.rA

  def serverInit(self, clientmsg):
    clientmsgs = clientmsg.split(',')
    self.b = random.randrange(30,101)
    self.rB = str(random.randrange(30,101))
    self.serverID = "Team6"
    self.clientID = clientmsgs[0]
    self.iv = 16 * '\x00'

    gbp = (self.g**self.b)%self.p
    rA = clientmsgs[1]
    msg = str(self.serverID)+ "," + rA + "," + str(gbp)+ "," + self.keyAB
    cipher = AES.new(self.keyAB,AES.MODE_CFB,self.iv)
    encryptedMsg = cipher.encrypt(msg)
    print msg
    return self.rB + "," + encryptedMsg + "," + self.iv

  def clientReply(self, servermsg):
    servermsgs = servermsg.split(',')
    self.iv = servermsgs[2]
    decryptor = AES.new(self.keyAB, AES.MODE_CFB, self.iv)
    plaintext = decryptor.decrypt(servermsgs[1])
    print plaintext
    msgs = plaintext.split(',')
    self.serverID = msgs[0]
    if self.serverID != "Team6":
      print "ServerID Mismatch!"
      return False
    if self.rA != msgs[1]:
      print "Nonce Mismatch!"
      return False
    B = int(msgs[2])
    self.keyS = str((B**self.a)%self.p).zfill(16)
    print self.keyS
    gap = self.g**self.a%self.p
    self.rB = servermsgs[0]
    myMsg = str(self.clientID)+ "," + self.rB + "," + str(gap)+ "," + self.keyAB
    cipher = AES.new(self.keyAB,AES.MODE_CFB,servermsgs[2])
    return cipher.encrypt(myMsg)

  def serverReply(self, clientmsg):
    decryptor = AES.new(self.keyAB, AES.MODE_CFB, self.iv)
    plaintext = decryptor.decrypt(clientmsg)
    print plaintext
    msgs = plaintext.split(',')
    if self.clientID != msgs[0]:
      print "Client ID Mismatch!"
      return False
    if self.rB != msgs[1]:
      print "Nonce Mismatch!"
      return False
    A = int(msgs[2])
    self.keyS = str((A**self.b)%self.p).zfill(16)
    print self.keyS

if __name__ == '__main__':
  c = Vpn()
  c.vpnServer()
  