import socket, threading
from authentication import auth
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Random import random

HOST = ''                 # Symbolic name meaning the local host
PORT = 61809              # Arbitrary non-privileged port


    




# Echo server program
def serverSocket():

  # Initialize authentication
  authServer = auth()

  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.bind((HOST, PORT))
  s.listen(1)
  while 1:
    conn, addr = s.accept()
    print 'Connected by', addr

    # Authentication protocol
    readFromSocket(conn)
    serverkey = authServer.serverInit(conn)

    # Multithreading
    t1 = threading.Thread(target=readFromSocket, args=(conn,))
    t1.start()
    t2 = threading.Thread(target=waitForInput, args=(conn,))
    t2.start()

# Echo client program
def clientSocket():

  # Initialize authentication
  authClient = auth()

  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((HOST, PORT))

  # Authentication protocol
  authClient.clientInit()
  message = authClient.rA + authClient.clienthash + authClient.keyAB
  s.send(message)

  # Multithreading
  t1 = threading.Thread(target=readFromSocket, args=(s,))
  t1.start()
  t2 = threading.Thread(target=waitForInput, args=(s,))
  t2.start()

def readFromSocket(s):
    while 1:
      data = s.recv(1024)
      if not data:
        break
      print 'Received', repr(data)
      # Echo  
      # conn.send(data)

def waitForInput(s):
  while 1:
    message = raw_input()
    s.send(message)
    # data = s.recv(1024)
    # print 'Received', repr(data)

if __name__ == '__main__':
    mode = raw_input("Are you a server(1) or a client(2)? ")
    if mode == "1":
      serverSocket()
    elif mode == "2":
      clientSocket()
    else: 
      print("Wrong option, terminating")
